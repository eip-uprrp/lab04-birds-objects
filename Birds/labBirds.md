# Objects in C++

In this lab you will be practing the creation and manipulation of objects of a class called `Bird`.  Before creating objects of any class you should be familiar with the class' documentation, which informs you, among other things, what abstract entity the class is trying to represent and the class' interface, i.e. the functions are available to manipulate the objects of the class.

Please take a look at the Bird class documentation [here]()

## Constructors

The first functions of a class interface that you should understand are its *constructor functions*. In C++, the constructor functions are called the same as the class. The Bird class has two constructors:

Bird (QWidget *parent=0)
Bird (int, EyeBrowType, QString, QString, QWidget *parent=0) 

Knowing the constructors helps you understand how you will create objects of the class. Whenever an object of class Bird is created one of the two constructor functions will be invoked, depending on the statement that you use for declaring the object. 

The first constructor `Bird (QWidget *parent=0)` is a function that can be called with 0 or 1 parameters. The class constructor that can be called with 0 parameters is the class' *default constructor*, i.e. it is the constructor that is called when we create an object using a statement such as this:

```
Bird hedwig;
```


Take a look at the documentation for the second constructor `Bird (int, EyeBrowType, QString, QString, QWidget *parent=0)`. It requires 4 or 5 arguments (the last argument is optional).  Therefore, to use this constructor, we would create an object using a statement such as this:

```
Bird nevermore(200, Bird::UPSET, "blue", "red");
```

## Setters

Classes provide functions to modify the values of object properties once the object has been created. These functions are called *setters* and there is usually one *setter* function for each of the properties of the class. The `Bird` class offers the following setters:

* void setEyeColor (QString)
* void setFaceColor (QString)
* void setEyebrow (EyeBrowType)
* void setSize (int)

The following example code creates an object ('dodo') of class Bird and then changes its size to `333`.

```
Bird dodo;
dodo.setSize(333);
```

## Getters

Classes also provide functions to *get* the value of an object's property. These functions are called *getters* and there is usually one *getter* function for each of the properties of the class. The `Bird` class offers the following getters:

* QString   getEyeColor ()
* QString  getFaceColor ()
* int getSize ()
* EyeBrowType getEyebrow ()


The following example code creates an object ('scooter') and prints its size.

```
Bird scooter;
cout << scooter.getSize();
```


## Additional functions

void MainWindow::addBird(int x, int y, Bird &b){
void MainWindow::addBird(Bird &b){


The class `Bird` is defined so that when an object of class `Bird` is added to the applications *MainWindow*, a bird cartoon will be shown in the window.  The *MainWindow* provides two functions to add a bird object to the window:


* void MainWindow::addBird(int x, int y, Bird &b);
* void MainWindow::addBird(Bird &b);

The following example code creates an object ('satchel') and adds it to the mainwindow `w` so that it will be drawn at position (100,150).

```cpp
MainWindow w;
Bird satchel;
w.addBird(200,200,satchel);
```

<img src="http://i.imgur.com/FH7xwDQ.png" width=400>


**IMPORTANT** You may create as many Bird objects as your computer memory allows, however, none of them will be painted unless you *add* them to the MainWindow using either of the `addBird` functions.


## Creating and manipulating objects

Download the ???? file, build the executable and run. The program will show a window with an empty drawing area (white background).

Let's practice creating birds.

Create a bird called "abelardo" using the default constructor. Then using the setters, set the properties of abelardo to the following: size = 200, eyeBrows = BUSHY, color = "yellow", eColor = "blue". Call the `addBird()` function of the MainWindow so that abelardo is drawn at position (100,100). Build and run your program.  Copy your code in the following box once your program runs.


The following figure illustrates the coordinate system used by Qt. 



Figure 1. A 400 x 300 window in Qt with some example positions.


The following code creates 30 birds with random properties and draws them in random positions.

```cpp
//
// Code snippet #1 
//

const int N = 30;
Bird birds[N] ;

srand(time(NULL)) ;

int x, y;
for (int i = 0; i < N ; i++ ){
    x = rand() % w.width();
    y = rand() % w.height();
    w.addBird(x, y, birds[i]);
}
```

Copy the code to the main function, build and run. You should see birds of many colors randomly drawn in the window.

Make the necessary changes so that the birds still have random y positions but are all *centered alligned* in the window:

<img src="http://i.imgur.com/Mqk32pU.png" width=400>

When you obtain the expected result, copy your code to the following text box.


Make the necessary changes so now only 16 birds are created (and painted) in a matrix of 4 rows and 4 columns.

<img src="http://i.imgur.com/egqLLR0.png" width=400>

When you obtain the expected result, copy your code to the following text box.


Now recopy `Code snippet #1 ` to the main function so that 100 randomly placed birds are drawn.  Test your code. 

Implement the function `FilterBirds01(birds, N)` so that when called only the birds with BUSHY eyeBrows are drawn. Hint: you will achieve this using a loop such as this:


```cpp
for (int i = 0; i < N ; i++ ){
    if (your condition is met)
       birds[i].hide();
}
```

<img src="http://i.imgur.com/GL6JBYO.png" width=400>

Implement the function `FilterBirds02(birds, N)` so that when called only the birds size smaller than or equal to 150 and whose eyes are red are drawn. Hint: you will achieve this using a loop such as this:


```cpp
for (int i = 0; i < N ; i++ ){
    if (your condition is met)
       birds[i].hide();
}
```

<img src="http://i.imgur.com/nc3IUVo.png" width=400>
