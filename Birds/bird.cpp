// 
// RAN [2014-06-02] 
//   - Changed the setEyebrow, setSize functions. Resize (+2,+2)
//   - EyeBrowType
// RAN [2014-06-03]
//   - TranslucentBackground for non-default constructor
//   - Some more comments

#include "bird.h"

///
/// Default constructor. The properties of the bird are set as follows:
/// * size: set to random value of either 50, 100, 150 or 200
/// * eyeBrow: set randomly to one of the five possible values
/// * color and eColor: set randomly to one of the five possible colors: 
/// "green", "blue", "red", "yellow", "white"
///
Bird::Bird(QWidget *parent) :
    QWidget(parent)
{
    QString prism[5] = {"green", "blue", "red", "yellow", "white"} ;
    size = randInt(1, 4) * 50 ;
    resize(size+2,size+2) ;
    move(0,0) ;
    eyeBrow = (EyeBrowType)randInt(0,3);
    color = prism[randInt(0,4)] ;
    eColor = prism[randInt(0,4)] ;
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("background:transparent;");
}

///
/// Constructor which accepts specification for s (size), eb (eyeBrow),
/// col (faceColor), eye (eyeColor)
///
Bird::Bird(int s, EyeBrowType eb, QString col, QString eye, QWidget *parent):
    QWidget(parent)
{
    resize(s+2,s+2) ;
    move(0,0) ;
    size = s; eyeBrow = eb; color=col ; eColor = eye ;
    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("background:transparent;");
}

///
/// Getter for the eyeColor.
///
QString  Bird::getEyeColor(){
    return eColor ;
}

///
/// Getter for the faceColor.
///
QString  Bird::getFaceColor(){
    return color ;
}

///
/// Setter for the eyeColor.
///
void  Bird::setEyeColor(QString eye){
    eColor = eye ;
}

///
/// Setter for the faceColor.
///
void  Bird::setFaceColor(QString col){
    color =   col;
}

///
/// Getter for the size.
///
int Bird::getSize(){
    return size ;
}

///
/// Getter for the Eyebrow.
///
Bird::EyeBrowType Bird::getEyebrow(){
    return eyeBrow ;
}

///
/// Setter for the size.
///
void Bird::setSize(int s){
  //  if(s <= 100 || s > 300)
  //     size = 100 ;
  //  else
    size = s;
    resize(size + 2, size + 2) ;
}

///
/// Setter for the Eyebrow.
///
void Bird::setEyebrow(EyeBrowType eb){
    if(eb < 0 || eb > 3)
        eyeBrow = UNI ;
    else eyeBrow = eb;
}

///
/// Converts a color specified as string to a Qt::GlobalColor
///
Qt::GlobalColor Bird::getColor(QString color){

    if(color=="red")
        return Qt::red ;
    else if (color == "green")
        return Qt::green ;
    else if (color == "yellow")
        return Qt::yellow ;
    else if(color == "blue")
        return Qt::blue ;
    else
        return Qt::white ;

}

///
/// Generates a random integer in the range [min, max]
///
int Bird::randInt(int min, int max){
    return qrand() % ((max +1 )- min) +  min ;
}

///
/// Assignment operator
///
Bird & Bird::operator=(Bird &b) {
    return *this;
}

///
/// This function is automatically invoked each time the widget or 
/// its parent receives a repaint signal.
///
void Bird::paintEvent(QPaintEvent *event) {
    QPainter p(this);

    QPen outlinePen(Qt::black);
    QPen eyebrowsPen(Qt::black) ;

    int ball_size = getSize() ;
    float half_ball = ball_size/2.0 ;
    float white_ball = ball_size / 4.0 ;
    float black_ball = white_ball * .5 ;
    float up_gap = ball_size * .10 ;
    float eyebrow_gap = ball_size *.18 ;

    outlinePen.setWidth(2);
    eyebrowsPen.setWidth(ball_size *.05);

    // Face
    p.setPen(outlinePen) ;
    p.setBrush(getColor(getFaceColor())) ;
    p.drawEllipse(0, 0, ball_size, ball_size);

    // Eye Balls
    p.setBrush(getColor(getEyeColor())) ;
    p.drawEllipse(half_ball-white_ball,ball_size/2-up_gap,white_ball,white_ball) ; //left eye
    p.drawEllipse(half_ball,ball_size/2-up_gap,white_ball,white_ball) ; //right eye

    // Eye iris
    p.setBrush(Qt::black) ;
    p.drawEllipse(half_ball-black_ball,ball_size/2-up_gap+(white_ball-black_ball)/2.0,black_ball,black_ball) ;
    p.drawEllipse(half_ball,ball_size/2-up_gap+(white_ball-black_ball)/2.0 ,black_ball,black_ball) ;

    // Eyebrow

    p.setPen(eyebrowsPen) ;

    if(eyeBrow == UNI){
        //Normal Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
    }
    else if(eyeBrow == ANGRY){

        // Angry Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
    }
    else if(eyeBrow == UPSET){
        // Angry Eyebrow
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap/2) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap/2, 3*white_ball,ball_size/2-eyebrow_gap) ;

    }
    else{
        // Hairy
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap, 3*white_ball,ball_size/2-eyebrow_gap) ;
        p.drawLine(white_ball, ball_size/2-eyebrow_gap, 2*white_ball, ball_size/2-eyebrow_gap+up_gap/2) ;
        p.drawLine(2*white_ball, ball_size/2-eyebrow_gap + up_gap/2, 3*white_ball,ball_size/2-eyebrow_gap) ;

    }

}
