#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "bird.h"
#include <QGraphicsScene>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addBird(int x, int y, Bird &b) ;
    void addBird(Bird &b) ;

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene ;
};

#endif // MAINWINDOW_H
