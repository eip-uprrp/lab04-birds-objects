// RAN [2014-06-03]
//   - Set initial size of the window to 1000,1000. Adjust the
//     mainView and scene accordingly.

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    resize(1000,1000);
    scene = new QGraphicsScene(this) ;
    ui->mainView->resize(width()*.95,height()*.95);
    scene->setSceneRect(QRectF(QPoint(0,0), QPoint(width()*.95,height()*.95))) ;
    ui->mainView->setScene(scene) ;
    ui->mainView->setAlignment((Qt::AlignLeft | Qt::AlignTop));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
}

void MainWindow::addBird(int x, int y, Bird &b){
    scene->addWidget(&b) ;
    b.move(x,y) ;
}

void MainWindow::addBird(Bird &b){
    scene->addWidget(&b) ;
}
