#ifndef BIRD_H
#define BIRD_H

#include <QWidget>
#include <QPainter>


/// A class to represent birds.
///
/// Bird is a subclass of QWidget. This means that the following QWidget functions
/// are also available for objects of the class Bird:
/// * move(int x, int y): to move the bird to position (x,y)
/// * x(), y(): get the x position, get the y() position
/// * hide(): to hide a bird that has been painted

class Bird : public QWidget
{
    Q_OBJECT
public:
    ///
    /// Enum type for the EyeBrow
    ///
    enum EyeBrowType {
        UNI,    /**< enum value 0 */
        ANGRY,  /**< enum value 1 */
        UPSET,  /**< enum value 2 */
        BUSHY   /**< enum value 3 */
    };
    
    explicit Bird(QWidget *parent = 0);
    Bird(int , EyeBrowType , QString , QString, QWidget *parent = 0) ;
    QString  getEyeColor() ;
    QString  getFaceColor() ;
    void  setEyeColor(QString) ;
    void  setFaceColor(QString) ;
    int getSize() ;
    EyeBrowType getEyebrow() ;
    void setSize(int) ;
    void setEyebrow(EyeBrowType) ;
    Qt::GlobalColor getColor(QString) ;

    Bird & operator=(Bird &b);
signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);

private:
    int size ;
    EyeBrowType eyeBrow ;
    QString color ;
    QString eColor ;
    int randInt(int min, int max) ;
};

#endif // BIRD_H
